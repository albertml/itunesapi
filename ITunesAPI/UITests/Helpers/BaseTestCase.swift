//
//  BaseTestCase.swift
//  UITests
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import XCTest

@testable import ITunesAPI

class BaseTestCase: XCTestCase {
  
  var app: XCUIApplication!
  
  override func setUp() {
    super.setUp()
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // Since UI tests are more expensive to run, it's usually a good idea
    // to exit if a failure was encountered
    continueAfterFailure = false
    
    app = XCUIApplication()
    
    // UI tests must launch the application that they test. Doing this in setup will
    // make sure it happens for each test method.
    app.launch()
    
    // In UI tests it’s important to set the initial state - such as interface orientation -
    // required for your tests before they run. The setUp method is a good place to do this.
  }
  
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  override func tearDown() {
    app = nil
    super.tearDown()
  }
  
  func dismissKeyboard() {
    
    // Dismiss keyboard by tapping its toolbar Done button.
    // Note that this toolbar is automatically created for us by IQKeyboardManager lib.
    let kbToolbarDoneButton = app.buttons["Toolbar Done Button"]
    XCTAssertTrue(kbToolbarDoneButton.exists && kbToolbarDoneButton.isHittable)
    kbToolbarDoneButton.tap()
  }
  
}
