//
//  BaseLoginFormTestCase.swift
//  UITests
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import XCTest

@testable import ITunesAPI

class BaseLoginFormTestCase: BaseTestCase {
  
  func fillEmailFormWithRegisteredEmailAddressAndHitNextButton() {
    
    // Find the Email field and supply the registered email address.
    let emailTextField = app.textFields["Email"]
    XCTAssertTrue(emailTextField.exists && emailTextField.isHittable)
    emailTextField.tap()
    emailTextField.typeText("example@domain.com")
    
    dismissKeyboard()
    
    let startButton = app.buttons["Next"]
    XCTAssertTrue(startButton.exists && startButton.isHittable)
    startButton.tap()
    
    // Same button, different accessibilityLabel.
    XCTAssertFalse(app.buttons["Next"].exists)
    XCTAssertTrue(app.buttons["Login"].exists)
  }
  
}
