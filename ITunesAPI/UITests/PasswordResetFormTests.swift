//
//  PasswordResetFormTests.swift
//  UITests
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import XCTest
import Nimble

@testable import ITunesAPI

class PasswordResetFormTests: BaseLoginFormTestCase {
  
  func test() {
    
    fillEmailFormWithRegisteredEmailAddressAndHitNextButton()
    
    let forgotPasswordButton = app.buttons["forgotPasswordButton"]
    XCTAssertTrue(forgotPasswordButton.exists && forgotPasswordButton.isHittable)
    forgotPasswordButton.tap()
    
    XCTAssertTrue(app.navigationBars["Reset Password"].exists)
    
    let emailTextField = app.textFields["emailTextField"]
    XCTAssertTrue(emailTextField.exists && emailTextField.isHittable)
    
    XCTAssertTrue(app.buttons["SEND"].exists)
    app.buttons["SEND"].tap()
    
    let alert = app.alerts["Password Reset Email Sent"]
    XCTAssertTrue(alert.waitForExistence(timeout: 3))
    alert.buttons["Ok"].tap()
  }
  
}
