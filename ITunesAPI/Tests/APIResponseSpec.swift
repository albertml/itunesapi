import Quick
import Nimble
import Mockingjay

import Firebase

@testable import ITunesAPI

class APIResponseSpec : QuickSpec {
  
  struct Post: Decodable, Identifiable, Equatable {
    
    enum CodingKeys: String, CodingKey {
      case id = "post_id"
      case title
      case commentCount = "comment_count"
      case author
    }
    
    let id: String
    let title: String
    let commentCount: Int
    let author: Author
    
  }
  
  struct Author: Decodable {
    let name: String
  }
  
  override func spec() {
    
    let payload = jsonDictionaryFromFile("apiresponse")
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    describe("errorContext property", closure: {
      
      context("when status indicates invalid credentials error", closure: {
        
        let data = try! JSONSerialization.data(
          withJSONObject: payload["error_invalid_credentials"]!
        )
        let instance = try! JSONDecoder().decode(APIResponse.self, from: data)
        
        // Any of the client or server errors.
        expect((400...599)).to(contain([instance.status.rawValue]))
        
        let errorContext = instance.errorContext
        
        it("should not be nil", closure: {
          expect(errorContext).toNot(beNil())
        })
        
        it("provides invalid credentials context", closure: {
          expect(errorContext?.message).to(equal("Invalid Credentials"))
          expect(errorContext?.errorCode).to(equal(APIResponseErrorCode.invalidCredentials))
          expect(errorContext?.status).to(equal(HTTPStatusCode.badRequest))
        })
        
      })
      
    })
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    describe("decodedValue function") {
      
      context("when data property contains Post data") {
        
        it("should return a Post model", closure: {
          let data = try! JSONSerialization.data(
            withJSONObject: payload["post"]!
          )
          let instance = try! JSONDecoder().decode(APIResponse.self, from: data)
          let post: Post! = instance.decodedValue()
          
          expect(post.id).to(equal("xyz123"))
          expect(post.title).to(equal("Hello Earth!"))
          expect(post.author.name).to(equal("Mark"))
        })
        
      }
      
      context("when data property contains a list of Post data", closure: {
        
        it("should return an array of Post models", closure: {
          let data = try! JSONSerialization.data(
            withJSONObject: payload["posts"]!
          )
          let instance = try! JSONDecoder().decode(APIResponse.self, from: data)
          let posts: [Post]? = instance.decodedValue(forKeyPath: "posts")
          
          expect(posts).toNot(beNil())
          expect(posts!.count).to(beGreaterThanOrEqualTo(0))

          let post = posts!.first!
          expect(post.id).to(equal("xyz123"))
          expect(post.title).to(equal("Hello Earth!"))
          expect(post.author.name).to(equal("Mark"))
        })
        
      })
      
      context("when status indicates a client or server error", closure: {
        
        it("should return nil", closure: {
          let data = try! JSONSerialization.data(
            withJSONObject: payload["error_invalid_credentials"]!
          )
          let instance = try! JSONDecoder().decode(APIResponse.self, from: data)
          let result: AnyDecodable? = instance.decodedValue()
          expect(result).to(beNil())
        })
        
      })
      
    }
    
  } // spec
  
}
