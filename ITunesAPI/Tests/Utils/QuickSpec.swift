import Quick
import ITunesAPI

extension QuickSpec {
  
  func jsonDictionaryFromFile(_ name: String) -> JSONDictionary {
    return ITunesAPI.jsonDictionaryFromFile(name, bundle: Bundle(for: type(of: self)))
  }
  
}
