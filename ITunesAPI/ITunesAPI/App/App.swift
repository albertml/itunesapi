//
//  App.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import GooglePlaces
import Valet
import Crashlytics
//import Firebase

/// This is our main application object. This holds instances of all the services available
/// in the app like the APIClient, AppUser, etc.
///
/// IMPORTANT:
/// - Defer creation of service instance up to the point where it's first needed.
///
class App {
  
  enum Environment: String {
    case staging
    case production
  }
  
  static let shared = App()
  
  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if STAGING
      return .staging
    #else
      return .production
    #endif
  }
  
  static let valet = Valet.valet(
    with: Identifier(nonEmpty: App.bundleIdentifier!)!,
    accessibility: .whenUnlocked
  )
  
  private(set) var api: APIClient!

  // MARK: Initialization
  
  init() {
    debugLog("env: \(App.environment.rawValue)")
  }
  
  func bootstrap(
    with application: UIApplication,
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
    api = APIClient(baseURL: URL(string: "https://itunes.apple.com")!)
  }
  
  func recordError(_ error: Error, info: [String: Any]? = nil) {
    debugLog(String(describing: error))
    
    if info != nil {
      debugLog("other info: \(String(describing: info!))")
    }
    
    Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: info)
  }

}

protocol AppService {}

extension AppService {
  
  var app: App {
    return App.shared
  }
  
}

// MARK: - App Info
extension App {
  
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }
  
  /// A dictionary, constructed from the bundle’s Info.plist file.
  static var info: [String: Any] {
    return Bundle.main.infoDictionary ?? [:]
  }
  
  static var displayName: String {
    return (self.info["CFBundleDisplayName"] as? String) ?? "ITunesAPI"
  }
  
  /// Alias for `CFBundleShortVersionString`.
  static var releaseVersion: String {
    return (self.info["CFBundleShortVersionString"] as? String) ?? "1.0"
  }
  
  /// Alias for `CFBundleVersion`.
  static var buildNumber: String {
    return (self.info["CFBundleVersion"] as? String) ?? "1"
  }
  
}

/// Use this for all App-level errors.
enum AppError: Error {
  case unauthorized(_ reason: String)
  case unknown
}

extension AppError: LocalizedError {
  
  var errorDescription: String? {
    switch self {
    case .unauthorized:
      return "Authorization required"
    default:
      return "Something went wrong"
    }
  }
  
  var failureReason: String? {
    switch self {
    case .unauthorized(let reason):
      return reason
    default:
      return "An unknown error has occured"
    }
  }
  
}
