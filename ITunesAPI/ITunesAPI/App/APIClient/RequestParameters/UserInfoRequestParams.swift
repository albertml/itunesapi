//
//  UserInfoRequestParams.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

/// Fill this object with values you want updated for the current user.
final class UserInfoRequestParams: APIRequestParameters {
  
  var firstName: String?
  var lastName: String?
  var country: String?
  var mobileNumber: String?
  var address: String?
  
}

extension UserInfoRequestParams: Codable {
  
  enum CodingKeys: String, CodingKey {
    case firstName = "first_name"
    case lastName = "last_name"
    case country
    case mobileNumber = "mobile_number"
    case address
  }
  
}
