//
//  APIClient.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire
import Valet

typealias APIClientResultClosure = (APIClientResult) -> Void
typealias APIClientSearchItunesClosure = ([Movies]?, Error?) -> Void

enum APIClientResult {
  case success(APIResponse)
  case failure(Error)
}

/// Our designated class that interfaces with the REST API. There should only be one instance
/// of this throughout the app's lifetime.
///
/// General guidelines:
/// - Group related methods into one class extension and separate it into its own file.
///   Name it like so: `APIClient+<GROUP_NAME_HERE>.swift`.
/// - Use `-httpRequestHeaders(withAuth: FALSE)` when calling endpoints that don't require access token.
///
/// Rules for naming endpoint functions.
/// - Always start with a verb. For example: `updatePassword`, `sendInvites`, `fetchComments`.
/// - If it's more than one word, just make sure you don't end up having a sentence. It should be
///   concise and simply tells the developer reading the code in the call site what it's all about.
///
class APIClient: AppService {
  
  /// The base URL of the REST API sans the version. E.g. `https://api.domain.com/`
  private(set) var baseURL: URL
  
  /// - parameters:
  ///   - baseURL: The base URL of the REST API sans the version. E.g. `https://api.domain.com/`
  ///
  public init(baseURL: URL) {
    self.baseURL = baseURL
  }
  
  public func endpointURL(_ resourcePath: String) -> URL {
    return baseURL.appendingPathComponent("/\(resourcePath)")
  }
  
  public func httpRequestHeaders(withAuth: Bool = true) -> HTTPHeaders {
    return [
      "Content-Type": "application/json"
    ]
  }
  
  /// This wraps the call to `Alamofire.request(...).apiResponse(result:)`.
  ///
  /// - parameters:
  ///   - resourcePath: The path of the API resource.
  ///   - method: The HTTP method for this API resource.
  ///   - version: Optional. Defaults to whatever the value of `self.version` property is.
  ///   - parameters: The parameters for this API resource. `nil` by default.
  ///   - encoding: The parameter encoding to use. Defaults to `URLEncoding.default`.
  ///   - headers: The HTTP headers. Defaults to calling `httpRequestHeaders(withAuth:)`.
  ///   - success: Accepts `APIResponse` instance.
  ///   - failure: Accepts `Error` instance.
  ///
  public func request(
    _ resourcePath: String,
    method: HTTPMethod,
    version: String? = nil,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil,
    success: @escaping (APIResponse) -> Void,
    failure: @escaping (Error) -> Void
    ) -> DataRequest {
    return Alamofire
      .request(
        endpointURL(resourcePath).absoluteString.removingPercentEncoding!,
        method: method,
        parameters: parameters,
        encoding: encoding,
        headers: headers
      )
      .apiResponse(completion: { (result) in
        switch result {
        case .success(let resp):
          success(resp)
        case .failure(let error):
          failure(error)
        }
      })
  }
}

// MARK: - APIClientError

enum APIClientError: Error {
  
  struct FailedRequestContext {
    let status: HTTPStatusCode
    let message: String
  }

  case failedRequest(FailedRequestContext)
  
  /// Indicates that a non-optional value of the given type was expected,
  /// but a null value was found.
  case dataNotFound(_ expectedType: Any.Type)
  
  case unknown
  
}

extension APIClientError: LocalizedError {
  
  var errorDescription: String? {
    switch self {
    case .failedRequest(let ctx):
      return ctx.message
    case .dataNotFound:
      return "Data expected from the server not found"
    default:
      return "An unknown error has occured"
    }
  }
  
  var failureReason: String? {
    switch self {
    case .failedRequest(let ctx):
      return "HTTPStatusCode: \(ctx.status); Message: \(ctx.message);"
    case .dataNotFound(let type):
      return "Expected \(String(describing: type)). Got nil instead."
    default:
      return "An unknown error has occured"
    }
  }
  
}

// MARK: - Alamofire.DataRequest

extension DataRequest {
  
  @discardableResult
  func apiResponse(
    queue: DispatchQueue? = nil,
    completion: @escaping APIClientResultClosure
    ) -> DataRequest {
    return self.responseData(queue: queue, completionHandler: { (response: DataResponse<Data>) in
      guard let statusCode = response.response?.statusCode else {
        let error = APIClientError.failedRequest(APIClientError.FailedRequestContext(
          status: .badRequest,
          message: "Can't get status code"
        ))
        App.shared.recordError(error)
        return completion(.failure(error))
      }
      
      guard let validData = response.data, validData.count > 0 else {
        App.shared.recordError(APIClientError.unknown)
        return completion(.failure(APIClientError.unknown))
      }
      
      guard response.result.error == nil else {
        App.shared.recordError(response.result.error!)
        return completion(.failure(response.result.error!))
      }
      
      do {
        switch statusCode {
        case 200...299:
          let model = try JSONDecoder().decode(APIResponse.self, from: validData)
          completion(.success(model))
        default:
          App.shared.recordError(APIClientError.unknown)
          completion(.failure(APIClientError.unknown))
        }
        
      } catch {
        App.shared.recordError(error)
        completion(.failure(error))
      }
    })
  }
  
}
