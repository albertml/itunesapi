//
//  APIClient+Itunes.swift
//  ITunesAPI
//
//  Created by Albert on 11/6/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Account
extension APIClient {
  
  /// Search movies from iTunes.
  @discardableResult
  func searchItunes(_ content: String, completion: @escaping APIClientSearchItunesClosure) -> DataRequest {
    return request("search?term=\(content)&country=au&media=movie&all", method: .get,
                   headers: httpRequestHeaders(withAuth: false),
                   success: { resp in
                    completion(resp.results, nil)
    },
                   failure: { error in
                    completion(nil, error)
    })

  }
}
