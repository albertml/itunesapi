//
//  APIResponse.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

struct APIResponse: Decodable {
  let resultCount: Int
  let results: [Movies]
}
