//
//  Helpers.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

public struct Helpers {}

// MARK: - Logging

public func debugLog(_ message: String, file: String = #file, line: Int = #line, function: String = #function) {
  #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("\(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
  #endif
  // Nothing to do if not debugging 
}

public func debugJSON(_ value: AnyObject) {
  #if DEBUG
    //
  #endif
}

// MARK: - File Management

public func jsonDictionaryFromFile(_ name: String, bundle: Bundle = Bundle.main) -> JSONDictionary {
  let path = bundle.path(forResource: name, ofType: "json")!
  let data = try! Data(contentsOf: URL(fileURLWithPath: path))
  let options = JSONSerialization.ReadingOptions.mutableContainers
  // swiftlint:disable:next force_cast
  return try! JSONSerialization.jsonObject(with: data, options: options) as! JSONDictionary
}

/// Check whether the provided string is a valid Email address or not.
public func isValidEmail(_ text: String) -> Bool {
  let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
  let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
  return predicate.evaluate(with: text)
}
