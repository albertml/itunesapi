//
//  StoryboardHelper.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

protocol StoryboardHelper {
  
  /// Name of the Storyboard file.
  static var name: String { get }
  
}

extension StoryboardHelper {
  
  static func controller(forScene sceneId: String) -> UIViewController {
    let storyboard = UIStoryboard(name: name, bundle: nil)
    return storyboard.instantiateViewController(withIdentifier: sceneId)
  }
  
}

// MARK:- Storyboard factory
protocol StoryboardInstantiable {
  static var storyboardName: String { get }
  static var storyboardIdentifier: String { get }
}

extension StoryboardInstantiable where Self: UIViewController {
  
  static var storyboardIdentifier: String {
    return String(describing: Self.self)
  }
  
  static func instantiate() -> Self? {
    let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    guard let controller = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier)
      as? Self else {
        return nil
    }
    return controller
  }
}
