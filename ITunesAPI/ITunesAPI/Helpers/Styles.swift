//
//  Styles.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

struct Styles {
  
  struct Colors {
    
    static let windowTint = #colorLiteral(red: 0.6980392157, green: 0.1490196078, blue: 0.568627451, alpha: 1)
    static let viewControllerBackground = UIColor.white
  
    /// Colors for Form elements like Button, TextField, etc.
    struct Form {
      // Add definitions to the designated extension.
    }
    
  }
  
}

extension Styles.Colors.Form {
  
  struct Button {
    static let primaryGradient = [#colorLiteral(red: 0.1843137255, green: 0.8470588235, blue: 0.8705882353, alpha: 1), #colorLiteral(red: 0.1803921569, green: 0.3960784314, blue: 0.8901960784, alpha: 1)]
  }
  
}
