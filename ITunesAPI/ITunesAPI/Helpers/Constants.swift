//
//  Constants.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: Any]

typealias S = R.string.localizable // swiftlint:disable:this type_name

/// Typealias for closures that take Void argument and return Void type.
typealias VoidClosure = () -> Void
/// Typealias for closures that take an optional Error argument and return a Void type.
typealias FailureClosure = (Error?) -> Void

struct Constants {
  
  struct Formatters {
    
    static let debugConsoleDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
      formatter.timeZone = TimeZone(identifier: "UTC")!
      return formatter
    }()
    
  }
  
}
