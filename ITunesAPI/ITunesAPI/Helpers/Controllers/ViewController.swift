//
//  ViewController.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  enum CloseButtonPosition {
    case left
    case right
  }
  
  /// Close button's position when this controller is presented modally. Defaults to `left`.
  var preferredCloseButtonPosition: CloseButtonPosition {
    return .left
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = Styles.Colors.viewControllerBackground
    setupNavBarItems()
  }
  
  /// A place for adding your custom navigation bar buttons.
  ///
  /// By default, a custom **Back** button will appear on the left side of the navigation bar
  /// if this controller isn't the root of its `navigationController`. On the other hand,
  /// if it's presented modally, we show a **Close** button.
  ///
  /// This method is called inside `viewDidLoad`.
  ///
  func setupNavBarItems() {
    if navigationController?.viewControllers.first != self {
      navigationItem.leftBarButtonItem = UIBarButtonItem(
        title: S.back(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
    } else if isPresentedModally {
      let button = UIBarButtonItem(
        title: S.close(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
      if preferredCloseButtonPosition == .left {
        navigationItem.leftBarButtonItem = button
      } else {
        navigationItem.rightBarButtonItem = button
      }
    }
  }
  
  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    if navigationController?.viewControllers.first != self {
      navigationController?.popViewController(animated: true)
    } else if isPresentedModally {
      dismiss(animated: true, completion: nil)
    }
  }
  
}
