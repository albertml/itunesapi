//
//  Valet.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//  

import Foundation
import Valet

extension Valet {
  
  /// Removes the previous value if new value set is Nil.
  func setString(_ value: String?, forKey: String) {
    if value == nil {
      removeObject(forKey: forKey)
    } else {
      set(string: value!, forKey: forKey)
    }
  }
  
  func getString(forKey: String) -> String? {
    guard self.canAccessKeychain() else {
      debugLog("Can't access KeyChain.")
      return nil
    }
    return self.string(forKey: forKey)
  }
  
  /// Removes the previous value if new value set is Nil.
  func setData(_ value: Data?, forKey: String) {
    if value == nil {
      removeObject(forKey: forKey)
    } else {
      set(object: value!, forKey: forKey)
    }
  }
  
  func getData(forKey: String) -> Data? {
    guard self.canAccessKeychain() else {
      debugLog("Can't access KeyChain.")
      return nil
    }
    return self.object(forKey: forKey)
  }
  
}
