//
//  UIWindow.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

extension UIWindow {
  
  var visibleViewController: UIViewController? {
    return UIWindow.visibleViewController(from: self.rootViewController)
  }
  
  static func visibleViewController(from vc: UIViewController?) -> UIViewController? {
    if let nc = vc as? UINavigationController {
      return UIWindow.visibleViewController(from: nc.visibleViewController)
    } else if let tc = vc as? UITabBarController {
      return UIWindow.visibleViewController(from: tc.selectedViewController)
    } else {
      if let pvc = vc?.presentedViewController {
        return UIWindow.visibleViewController(from: pvc)
      } else {
        return vc
      }
    }
  }
  
  func setRootViewControllerAnimated(
    _ controller: UIViewController?,
    duration: TimeInterval = 0.25,
    options: UIView.AnimationOptions = .transitionCrossDissolve,
    completion: ((_ finished: Bool) -> Void)? = nil
  ) {
    UIView.transition(with: self, duration: duration, options: options, animations: {
      self.rootViewController = controller
    }, completion: completion)
  }
  
}
