//
//  UIImageView.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

extension UIImageView {
  
  public func setImageWithURL(
    _ url: URL,
    filter: ImageFilter? = nil,
    placeholder: UIImage? = nil,
    completion: ((DataResponse<UIImage>) -> Void)? = nil
  ) {
    af_setImage(
      withURL: url,
      placeholderImage: placeholder,
      filter: filter,
      imageTransition: .crossDissolve(0.3),
      completion: {
        (response: DataResponse<UIImage>) in
        if let error = response.result.error {
          print(error.localizedDescription)
        }
        completion?(response)
    })
  }
  
}
