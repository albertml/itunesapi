//
//  UIDevice.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

extension UIDevice {
  
  fileprivate static let keychainKey = "device_id"
  
  static var uniqueId: String {
    // TODO: Handle inaccessible keychain scenario.
    guard App.valet.canAccessKeychain() else {
      preconditionFailure("Keychain is not accessible.")
    }
    if App.valet.containsObject(forKey: keychainKey) {
      return App.valet.string(forKey: keychainKey)!
    }
    let newDeviceId = UIDevice.current.identifierForVendor!.uuidString
    App.valet.set(string: newDeviceId, forKey: keychainKey)
    return newDeviceId
  }
  
}
