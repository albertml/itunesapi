//
//  UIViewController.swift
//  ITunesAPI
//
//  Created by Joash on 26/02/2019.
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

protocol KeyboardEditable {
  
  func keyboardWillShowOrHideHandler(_ notification: Notification)
  
}

extension KeyboardEditable where Self: UIResponder {
  
  // MARK: - Interactive Keyboard Dismissal For UIScrollView
  // Ref: https://realm.io/news/tmi-scrollview-for-keyboards/
  
  /// Register to be notified if the keyboard is changing size
  func addKeyboardVisibilityEventObservers() {
    let nc = NotificationCenter.default
    _ = nc.addObserver(
      forName: UIResponder.keyboardWillShowNotification,
      object: nil,
      queue: OperationQueue.main) { [weak self] (notif) in
        self?.keyboardWillShowOrHideHandler(notif)
    }
    _ = nc.addObserver(
      forName: UIResponder.keyboardWillHideNotification,
      object: nil,
      queue: OperationQueue.main) { [weak self] (notif) in
        self?.keyboardWillShowOrHideHandler(notif)
    }
  }
  
}

protocol ScrollableController: KeyboardEditable {
  
  var scrollView: UIScrollView { get set }
  
}

extension ScrollableController where Self: UIViewController {
  
  func keyboardWillShowOrHideHandler(_ notification: Notification) {
    
    // Pull a bunch of info out of the notification
    if let userInfo = notification.userInfo,
      let endValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey],
      let durationValue = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] {
      
      // Transform the keyboard's frame into our view's coordinate system
      let endRect = view.convert((endValue as AnyObject).cgRectValue, from: view.window)
      
      // Find out how much the keyboard overlaps the scroll view
      // We can do this because our scroll view's frame is already in our view's coordinate system
      let keyboardOverlap = scrollView.frame.maxY - endRect.origin.y
      
      // Set the scroll view's content inset to avoid the keyboard
      // Don't forget the scroll indicator too!
      scrollView.contentInset.bottom = keyboardOverlap
      scrollView.scrollIndicatorInsets.bottom = keyboardOverlap
      
      let duration = (durationValue as AnyObject).doubleValue
      UIView.animate(withDuration: duration!, delay: 0, options: .beginFromCurrentState, animations: {
        self.view.layoutIfNeeded()
      }, completion: nil)
    }
  }
  
}

extension UIViewController {
  
  var isPresentedModally: Bool {
    return presentingViewController != nil ||
      navigationController?.presentingViewController?.presentedViewController === navigationController ||
      tabBarController?.presentingViewController is UITabBarController
  }
  
}
