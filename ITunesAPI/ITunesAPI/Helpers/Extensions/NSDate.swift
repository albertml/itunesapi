//
//  NSDate.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

extension Date {
  
  func dblog() -> String {
    return Constants.Formatters.debugConsoleDateFormatter.string(from: self)
  }
  
}
