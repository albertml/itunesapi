//
//  AppDelegate.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
//import Firebase
import IQKeyboardManagerSwift
import AlamofireNetworkActivityIndicator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
  
  var window: UIWindow?
  
  override init() {}
  
  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
  ) -> Bool {
    
    // Make sure you have the corresponding `GoogleService-Info.plist` files before
    // uncommenting the next line. Should include both the production and the staging files.
    // FirebaseApp.configure()
    
    App.shared.bootstrap(with: application, launchOptions: launchOptions)
    
    IQKeyboardManager.shared.enable = true
    NetworkActivityIndicatorManager.shared.isEnabled = true
    
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = UIColor.black
    window?.tintColor = Styles.Colors.windowTint
    updateRootViewController()
    loadAppearancePreferences()
    window?.makeKeyAndVisible()
    
    return true
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // TODO: Manage this properly.
    application.applicationIconBadgeNumber = 0
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {}
  
  func applicationWillResignActive(_ application: UIApplication) {}
  
  func applicationWillEnterForeground(_ application: UIApplication) {}
  
  func applicationWillTerminate(_ application: UIApplication) {
    let userDefaults = UserDefaults()
    userDefaults.setValue(Date().toString(), forKeyPath: "last-open")
  }
  
}

// MARK: - RootViewController Management
extension AppDelegate {
  
  func updateRootViewController() {
    self.window?.setRootViewControllerAnimated(R.storyboard.home().instantiateInitialViewController())
    let splitViewController = window!.rootViewController as! UISplitViewController
    splitViewController.preferredDisplayMode = .allVisible
    let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
    navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
    splitViewController.delegate = self
  }
  
}

// MARK: - User Notifications
extension AppDelegate {
  
  /// Receives the device token needed to deliver remote notifications. Device tokens can change,
  /// so your app needs to re-register every time it is launched and pass the received token back
  /// to your server. Device tokens always change when the user restores backup data to a new
  /// device or computer or re-installs the operating system.
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    let token = tokenParts.joined()
    debugLog("Device Token: \(token)")
  }
  
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    // you should process the error object appropriately and disable any features related to
    // remote notifications. Because notifications are not going to be arriving anyway, it is
    // usually better to degrade gracefully and avoid any unnecessary work needed to process
    // or display those notifications.
    debugLog("Failed to register: \(error)")
    App.shared.recordError(error)
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    debugLog(String(describing: userInfo))
    // App.shared.remoteNotificationManager.storeNotification(userInfo)
  }
  
}

extension AppDelegate {
  // MARK: - Split view
  func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
    guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
    guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
    if topAsDetailController.detailItem == nil {
      // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
      return true
    }
    return false
  }
}
