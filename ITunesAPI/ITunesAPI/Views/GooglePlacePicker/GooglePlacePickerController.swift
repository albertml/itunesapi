//
//  GooglePlacePickerController.swift
//  ITunesAPI
//
//  Created by Joash on 26/02/2019.
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import GooglePlaces
import SVProgressHUD

typealias GooglePlacePickerSelectionCallback = ((
  GooglePlacePickerController,
  GMSPlace,
  (photo: UIImage, attribution: NSAttributedString?)
  ) -> Void)

class GooglePlacePickerController: UIViewController {
  
  var searchKeyword: String?
  var emptyResultsView: UIView?
  
  var onPlaceSelection: GooglePlacePickerSelectionCallback?
  
  var resultsViewController: GooglePlaceSearchResultsController?
  
  fileprivate(set) var selectedPlace: GMSPlace?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.white
    
    setupResults()
  }
  
  fileprivate func setupResults() {
  resultsViewController = GooglePlaceSearchResultsController()
  resultsViewController?.view.backgroundColor = view.backgroundColor
  resultsViewController?.delegate = self
  resultsViewController?.emptyResultsView = emptyResultsView
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBar.isTranslucent = false
  }
  
}

extension GooglePlacePickerController: GooglePlaceSearchResultsControllerDelegate {
  
  func resultsController(_ resultsController: GooglePlaceSearchResultsController, didSelect place: GMSPlace) {
    selectedPlace = place
  }
  
  func resultsController(_ resultsController: GooglePlaceSearchResultsController, didFailWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
    SVProgressHUD.showDismissableError(with: error.localizedDescription)
  }
  
  // Turn the network activity indicator on and off again.
  func didRequestGooglePlaces(_ viewController: GooglePlaceSearchResultsController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }
  
  func didUpdateGooglePlaces(_ viewController: GooglePlaceSearchResultsController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
  
}

