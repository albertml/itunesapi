//
//  MDButton.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import Material
import ChameleonFramework

class MDButton: Button {
  
}

/// A button that's intended for use in a form as its main action button. It has a bit of
/// distinct appearance making it stand out in a form possibly crowded with several
/// other components.
///
/// NOTE: You'd normally need only one of this in any given form.
class MDPrimaryButton: MDButton {
  
  override func prepare() {
    super.prepare()
    
    cornerRadiusPreset = .cornerRadius8
    
    backgroundColor = UIColor(
      gradientStyle: .leftToRight,
      withFrame: bounds,
      andColors: Styles.Colors.Form.Button.primaryGradient
    )
  }
  
}
