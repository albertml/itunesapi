//
//  FormTextField.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

@IBDesignable
class FormTextField: UITextField {
  
  @IBInspectable var borderWidth: CGFloat = 1 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  @IBInspectable var textBorderColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3153360445) {
    didSet {
      layer.borderColor = textBorderColor.cgColor
    }
  }
  
  @IBInspectable var cornerRadius: CGFloat = 4 {
    didSet {
      layer.cornerRadius = cornerRadius
    }
  }
  
  @IBInspectable var insetX: CGFloat = 16
  @IBInspectable var insetY: CGFloat = 0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    afterInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    afterInit()
  }
  
  func afterInit() {
    font = UIFont.systemFont(ofSize: 16)
    textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5959706764)
  }
  
  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }
  
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }
  
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: insetX, dy: insetY)
  }
  
}
