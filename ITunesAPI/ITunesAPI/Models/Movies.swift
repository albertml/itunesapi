//
//  Movies.swift
//  ITunesAPI
//
//  Created by Albert on 11/6/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

// MARK: - Movies
struct Movies: APIModel, Codable, Equatable {
  let id: Double?
  let title: String?
  let artist: String?
  let artworkUrl: String?
  let price: Float?
  let genre: String?
  let longDescription: String?
  
  enum CodingKeys: String, CodingKey {
    case id = "trackId"
    case title = "trackName"
    case artist = "artistName"
    case artworkUrl = "artworkUrl100"
    case price = "trackPrice"
    case genre = "primaryGenreName"
    case longDescription
  }
}


