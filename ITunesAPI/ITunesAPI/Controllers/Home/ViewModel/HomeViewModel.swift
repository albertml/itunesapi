//
//  HomeViewModel.swift
//  ITunesAPI
//
//  Created by Albert on 11/6/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class HomeViewModel: AppService {
  
  var movies: [Movies] = []
  
  var pageTitle: String {
    return "Itunes Movies"
  }
  
  func searchItunes(content: String, completion: @escaping APIClientSearchItunesClosure) {
    app.api.searchItunes(content, completion: searchItunesHandler(completion))
  }
  
  private func searchItunesHandler(_ completion: @escaping APIClientSearchItunesClosure) -> APIClientSearchItunesClosure {
    return { (movies, error) in
      guard error == nil else {
        return completion(nil, error)
      }
      completion(movies, nil)
    }
  }
}

extension HomeViewModel {
  func cellViewModel(for indexPath: IndexPath) -> MoviesCellViewModel {
    return MoviesCellViewModel(movies: movies[indexPath.row])
  }
}
