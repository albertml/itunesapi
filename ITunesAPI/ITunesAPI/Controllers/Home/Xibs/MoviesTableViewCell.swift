//
//  MoviesTableViewCell.swift
//  ItunesAPI
//
//  Created by Albert on 30/07/2019.
//  Copyright © 2019 Alberto Gaudicos Jr. All rights reserved.
//

import UIKit

class MoviesCellViewModel {
  private var movies: Movies
  init(movies: Movies) {
    self.movies = movies
  }
  
  var movieTitle: String {
    return movies.title ?? ""
  }
  
  var moviePrice: String {
    if let price = movies.price {
      return "$" + String(price)
    }
    return "$0"
  }
  
  var movieGenre: String {
    return movies.genre ?? ""
  }
  
  var movieArtWork: URL? {
    return URL(string: movies.artworkUrl ?? "")
  }
  
  var movieArtist: String {
    return movies.artist ?? ""
  }
  
  var movieDescription: String {
    return movies.longDescription ?? ""
  }
}

class MoviesTableViewCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var imgArtWork: UIImageView!
    
    // MARK: Properties
  
  var viewModel: MoviesCellViewModel? {
    didSet {
      update()
    }
  }
  
  private func update() {
    lblTrackName.text = viewModel?.movieTitle
    lblPrice.text = viewModel?.moviePrice
    lblGenre.text = viewModel?.movieGenre
    guard let artWorkUrl = viewModel?.movieArtWork else { return }
    imgArtWork.setImageWithURL(artWorkUrl)
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
