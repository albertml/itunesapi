//
//  HomeViewController+Extensions.swift
//  ITunesAPI
//
//  Created by Albert on 11/7/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

extension HomeTableViewController {
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return homeViewModel.movies.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let movieCell: MoviesTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
    movieCell.viewModel = self.homeViewModel.cellViewModel(for: indexPath)
    return movieCell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.performSegue(withIdentifier: R.segue.homeTableViewController.showDetail, sender: self)
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let lastViewedView: LastViewedTableViewCell = tableView.dequeueReusableNoIndexCell()
    lastViewedView.showLastViewed()
    return lastViewedView
  }
}
