//
//  DetailViewController.swift
//  ITunesAPI
//
//  Created by Albert on 11/6/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  // MARK: Properties
  
  @IBOutlet weak var lblArtist: UILabel!
  @IBOutlet weak var lblTrackName: UILabel!
  @IBOutlet weak var detailDescriptionLabel: UILabel!
  @IBOutlet weak var imgArtwork: UIImageView!
  @IBOutlet weak var lblPrice: UILabel!
  @IBOutlet weak var lblGenre: UILabel!
  
  var viewModel: MoviesCellViewModel?
  
  func configureView() {
    if let nnViewModel = viewModel {
      self.title = nnViewModel.movieTitle
      self.lblTrackName.text = nnViewModel.movieTitle
      self.lblArtist.text = "Artist: " + nnViewModel.movieArtist
      self.lblGenre.text = "Genre: " + nnViewModel.movieGenre
      self.lblPrice.text = "Price: " + nnViewModel.moviePrice
      self.detailDescriptionLabel.text = nnViewModel.movieDescription
      guard let artWorkUrl = nnViewModel.movieArtWork else { return }
      self.imgArtwork.setImageWithURL(artWorkUrl)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    configureView()
  }
  
  var detailItem: NSDate? {
    didSet {
      // Update the view.
      configureView()
    }
  }
}
