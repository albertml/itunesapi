//
//  HomeTableViewController.swift
//  ITunesAPI
//
//  Created by Albert on 11/6/19.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeTableViewController: UITableViewController {
  
  let homeViewModel = HomeViewModel()
  var isFirstLoad = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigationButton()
    setupViews()
    fetchMovies(searchText: "star")
  }
  
  private func setupViews() {
    tableView.register(MoviesTableViewCell.self)
    tableView.register(LastViewedTableViewCell.self)
    tableView.tableFooterView = UIView()
    tableView.estimatedRowHeight = 80.0
  }
  
  func setupNavigationButton() {
    // Setup navigation appearance
    self.title = self.homeViewModel.pageTitle
    self.navigationController?.navigationBar.barTintColor = UIColor.purple
    let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    navigationController?.navigationBar.titleTextAttributes = textAttributes
    
    // Setup search controller
    let search = UISearchController(searchResultsController: nil)
    search.searchResultsUpdater = self
    search.searchBar.tintColor = .white
    search.searchBar.setImage(R.image.ic_search(), for: UISearchBar.Icon.search, state: .normal)
    search.searchBar.setImage(R.image.ic_clear(), for: UISearchBar.Icon.clear, state: .normal)
    if #available(iOS 11.0, *) {
      self.navigationController?.navigationBar.prefersLargeTitles = true
      self.navigationItem.hidesSearchBarWhenScrolling = false
      self.navigationItem.searchController = search
    } else {
      // Fallback on earlier versions
    }
    
  }
  
  private func fetchMovies(searchText: String) {
    homeViewModel.searchItunes(content: searchText) { (movies, error) in
      guard let nnMovies = movies, error == nil else { return }
      self.homeViewModel.movies = nnMovies
      self.tableView.reloadData()
      if self.isFirstLoad == false {
        self.isFirstLoad = true
        if UIDevice.current.userInterfaceIdiom == .phone { return }
        self.performSegue(withIdentifier: R.segue.homeTableViewController.showDetail, sender: nil)
      }
    }
  }
  
  private func viewDetail(segue: UIStoryboardSegue, indexPath: IndexPath) {
    let itemDetailModule = (segue.destination as! UINavigationController).topViewController as! DetailViewController
    itemDetailModule.viewModel = self.homeViewModel.cellViewModel(for: indexPath)
    itemDetailModule.navigationItem.leftBarButtonItem = itemDetailModule.splitViewController?.displayModeButtonItem
    itemDetailModule.navigationItem.leftItemsSupplementBackButton = true
  }
}

extension HomeTableViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    // Check if search text is not nil or empty
    guard let searchText = searchController.searchBar.text else { return }
    if searchText.isEmpty { return }
    fetchMovies(searchText: searchText)
  }
}

extension HomeTableViewController {
  // MARK: - Segues
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == R.segue.homeTableViewController.showDetail.identifier {
      if let indexPath = tableView.indexPathForSelectedRow {
        viewDetail(segue: segue, indexPath: indexPath)
      } else {
        viewDetail(segue: segue, indexPath: IndexPath(item: 0, section: 0))
      }
    }
  }
}
