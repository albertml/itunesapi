//
//  AppDelegate+Appearance.swift
//  ITunesAPI
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

// All UIAppearance configurations should all go in here.

// MARK: - Appearance
extension AppDelegate {
  
  func loadAppearancePreferences() {
    loadNavBarAppearancePrefs()
    loadBarButtonItemAppearancePrefs()
    loadTableViewCellAppearancePrefs()
  }
  
  private func loadNavBarAppearancePrefs() {
    // Setup navigation bar global appearance
    if #available(iOS 11.0, *) {
      UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 22)]
    } else {
      // Fallback on earlier versions
    }
    UINavigationBar.appearance().tintColor = .white
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search Movies", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
  }
  
  private func loadBarButtonItemAppearancePrefs() {
    /*
    let barButton = UIBarButtonItem.appearance(whenContainedInInstancesOf: [
      ...
    ])
    barButton.setTitleTextAttributes([
        ...
      ],
      for: .normal
    )
    barButton.setTitleTextAttributes([
        ...
      ],
      for: .selected
    )
    */
  }
  
  private func loadTableViewCellAppearancePrefs() {
    /*
    let cell = UITableViewCell.appearance(whenContainedInInstancesOf: [...])
    cell.tintColor = ...
    */
  }
  
}
