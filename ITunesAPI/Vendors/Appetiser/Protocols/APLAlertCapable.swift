import UIKit

public protocol APLAlertCapable { }

public extension APLAlertCapable where Self: UIViewController {
  
  public func showInfoAlert(_ title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    present(alert, animated: true, completion: nil)
  }
  
  func showErrorAlert(message: String) {
    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    present(alert, animated: true, completion: nil)
  }
  
  func showNoInternetAlert() {
    showInfoAlert("No Internet Connection", message: "Make sure your device is connected to the internet.")
  }
  
}
